include 'emu8086.inc'
#make_com#
org 100h

mulai: jmp input
msg1 db "SELAMAT DATANG DI PROGRAM MENGHITUNG KUBUS $"  
rusuk db "Masukkan Rusuk: $"
hasil1 db "Volume kubus: $"
hasil2 db "Luas Permukaan kubus: $"
hasil3 db "Keliling permukaan kubus: $"
penutup db "Program Telah selesai $"

sisi dw 6
jmlhr dw 12

irusuk dw ?
hvolume dw ?
hluas dw ?
hkeliling dw ?

input: 
    mov dx,offset msg1
    mov ah,09h
    int 21h
    
    putc 13
    putc 10

    mov dx,offset rusuk
    mov ah,09h
    int 21h
    
    call scan_num
    mov rusuk,cx

    putc 13
    putc 10

volume:      
    mov ax,rusuk
    mov bx,rusuk
    imul ax
    mul bx
    push ax
    
    putc 13
    putc 10
     
    mov dx,offset hasil1
    mov ah,09h
    int 21h
    
    pop ax
    mov hvolume,ax
    
    call print_num 
    
    putc 13
    putc 10
    
luas:
    mov ax,rusuk
    mov bx,sisi
    imul ax
    mul bx
    push ax
    
    mov dx,offset hasil2
    mov ah,09h
    int 21h
    
    pop ax
    mov hluas,ax
    
    call print_num 
    
    putc 13
    putc 10
        
keliling:
    mov ax,rusuk
    mov bx,jmlhr
    mul bx
    push ax
    
    mov dx,offset hasil3
    mov ah,09h
    int 21h
    
    pop ax
    mov hkeliling,ax
    
    call print_num
    
    putc 13
    putc 10

akhir:
    putc 13
    putc 10
    
    mov dx,offset penutup
    mov ah,09h
    int 21h 
        
    mov ah,0
    int 16h  

ret

define_scan_num
define_print_num
define_print_num_uns 
 
END